# TinyBook Service
Facilitates the booking of deliveries via the Tiny Mile fleet of robots.

`tinybook` operates around three resources: `Robot`s, `Quote`s, and `Delivery`s. Robots make deliveries within a specific `DeliveryZone`. To create a delivery, you need to first make a quote (which makes sure that your pickUp and dropOff locations are within the delivery zone), then pass that quote to the delivery endpoint which assigns the first free robot and sets their status to `delivering`.
