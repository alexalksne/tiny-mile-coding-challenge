import express from 'express';
import logger from 'morgan';
import helmet from 'helmet';
import Debug from 'debug';
import { Connection } from 'typeorm';

import { Env } from '.';
import healthcheck from './resources/healthcheck/routes';
import robots from './resources/robots/routes';
import quotes from './resources/quotes/routes';
import deliveries from './resources/deliveries/routes';
import landing from './resources/landing/routes';
import notFound from './middleware/not-found.middleware';
import error from './middleware/error.middleware';
import CoreHealthcheckService from './resources/healthcheck/services/core-healthcheck.service';
import PostgresHealthcheckService from './resources/healthcheck/services/postgres-healthcheck.service';
import RobotService from './resources/robots/services/robot.service';
import { Robot } from './resources/robots/models/robot.model';
import QuoteService from './resources/quotes/services/quote.service';
import { Quote } from './resources/quotes/models/quote.model';
import { DeliveryZone, DeliveryZoneOrigin } from './resources/quotes/models/delivery-zone.model';
import DeliveryService from './resources/deliveries/services/delivery.service';
import { Delivery } from './resources/deliveries/models/delivery.model';

export type Class = { new(...args: any[]): any; name: string; };
type Services = {
  [key: string]: any;
}

const wireUpServices = (env: Env, connection: Connection): Services => {
  const {
    SERVICE_NAME,
    DELIVERY_ZONE_ORIGIN_LAT,
    DELIVERY_ZONE_ORIGIN_LNG,
    DELIVERY_ZONE_RADIUS_IN_METERS,
    DELIVERY_RATE_CENTS_PER_KM
  } = env;

  return {
    CoreHealthcheckService: new CoreHealthcheckService(
      Debug(`${SERVICE_NAME}:core-healthcheck-service`),
      new PostgresHealthcheckService(
        Debug(`${SERVICE_NAME}:postgres-healthcheck-service`),
        connection
      )
    ),
    RobotService: new RobotService(
      Debug(`${SERVICE_NAME}:robot-service`),
      connection.getRepository(Robot)
    ),
    QuoteService: new QuoteService(
      Debug(`${SERVICE_NAME}:quote-service`),
      connection,
      connection.getRepository(Quote),
      new DeliveryZone(
        new DeliveryZoneOrigin(
          DELIVERY_ZONE_ORIGIN_LAT,
          DELIVERY_ZONE_ORIGIN_LNG
        ),
        DELIVERY_ZONE_RADIUS_IN_METERS
      ),
      DELIVERY_RATE_CENTS_PER_KM
    ),
    DeliveryService: new DeliveryService(
      Debug(`${SERVICE_NAME}:delivery-service`),
      connection,
      connection.getRepository(Quote),
      connection.getRepository(Robot),
      connection.getRepository(Delivery),
    )
  };
}

const appFactory = (env: Env, connection: Connection) => {
  const services = wireUpServices(env, connection);
  const getService = (klass: Class) => services[klass.name];

  const app = express()
  .set('env', env)
  .set('dbConnection', connection)
  .set('getService', getService)
  .set('json spaces', 2)
  .set('views', null)
  .set('view cache', false)
  .use(helmet())
  .use(logger('short'))
  .use(express.json())
  .use(express.urlencoded({ extended: false }))
  .use('/', landing)
  .use('/healthcheck', healthcheck)
  .use('/robots', robots)
  .use('/quotes', quotes)
  .use('/deliveries', deliveries)
  .use(notFound)
  .use(error);

  return app;
}

export { appFactory };
