import 'reflect-metadata';
import { Expose, Type } from 'class-transformer';
import { USER } from '../../../constants';

export class HealthcheckDependency {
  @Expose({ groups: [USER] })
  ok!: boolean;

  @Expose({ groups: [USER] })
  type!: 'database' | 'api';

  @Expose({ groups: [USER] })
  name!: string;
}

/**
 * @openapi
 *  components:
 *    schemas:
 *      GetHealthcheckResponse:
 *        description: A detailed report covering the health of the system
 *        type: object
 *        properties:
 *          ok:
 *            description: Whether the entire system is healthy. If `false` then at least one key dependency of this service is down.
 *            type: boolean
 *            example: true
 *          dependencies:
 *            description: A list of dependencies required by this service
 *            type: array
 *            items:
 *              type: object
 *              properties:
 *                ok:
 *                  description: Whether the dependency is healthy
 *                  type: boolean
 *                  example: true
 *                type:
 *                  description: The kind of dependency
 *                  type: string
 *                  enum:
 *                  - database
 *                  - api
 *                  example: database
 *                name:
 *                  description: A human-readable name describing this service
 *                  type: string
 *                  example: postgres
 */
export class Healthcheck {
  @Expose({ groups: [USER] })
  ok!: boolean;

  @Expose({ groups: [USER] })
  @Type(() => HealthcheckDependency)
  dependencies!: HealthcheckDependency[];
}
