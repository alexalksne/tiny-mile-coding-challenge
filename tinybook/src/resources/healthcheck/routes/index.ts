import { Router } from 'express';
import { Healthcheck } from '../models/healthcheck.model';
import CoreHealthcheckService from '../services/core-healthcheck.service';
import { instanceToPlain } from 'class-transformer';
import { USER } from '../../../constants';

const router = Router();

/**
 * @openapi
 *  healthcheck:
 *    get:
 *      summary: Run healthcheck
 *      description: Returns a breakdown of the health of this service
 *      responses:
 *        200:
 *          description: The service is either fully-, or semi-, healthy
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/GetHealthcheckResponse'
 */
router.get('/', async function (req, res, next) {
  const service:CoreHealthcheckService = req.app.get('getService')(CoreHealthcheckService);

  const healthcheck:Healthcheck = await service.healthcheck();

  res.json(instanceToPlain(healthcheck, { groups: [USER] }));
});

export default router;
