import { Debugger } from 'debug';
import { Connection } from 'typeorm';
import { HealthcheckDependency } from '../models/healthcheck.model';

export class PostgresHealthcheckService {
    constructor(private debug: Debugger, private connection: Connection){};

    async healthcheck(): Promise<HealthcheckDependency> {
        let ok = false;
        try {
            await this.connection.query('SELECT COUNT(*) FROM information_schema.tables');
            ok = true;
        } catch(err: any) {
            this.debug(err);
        }

        const healthcheck:HealthcheckDependency = {
            ok,
            type: 'database',
            name: 'postgres'
        }

        return healthcheck;
    }
}

export default PostgresHealthcheckService;
