import { Debugger } from 'debug';
import { Healthcheck } from '../models/healthcheck.model';
import PostgresHealthcheckService from './postgres-healthcheck.service';

class CoreHealthcheckService {
    constructor(public debug: Debugger, public postgresHealthcheckService: PostgresHealthcheckService){};

    async healthcheck(): Promise<Healthcheck> {
        const healthcheck = new Healthcheck();
        healthcheck.dependencies = [
            await this.postgresHealthcheckService.healthcheck()
        ]

        healthcheck.ok = healthcheck.dependencies.every(d => d.ok);

        return healthcheck;
    }
}

export default CoreHealthcheckService;
