import { Router } from 'express';
import { instanceToPlain, plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { BadRequest } from 'http-errors';
import QuoteService, { DropOffOutOfDeliveryZoneException, PickUpOutOfDeliveryZoneException } from '../services/quote.service';
import { Quote } from '../models/quote.model';
import { ADMIN, CREATE, USER } from '../../../constants';

const router = Router();

/**
 * @openapi
 *  quotes:
 *    get:
 *      summary: Get quotes
 *      description: Gets all quotes in the system
 *      responses:
 *        200:
 *          description: An array of all the quotes
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/GetQuotesResponse'
 */
router.get('/', async function (req, res) {
  const service:QuoteService = req.app.get('getService')(QuoteService);

  const quotes = await service.getAll();

  res.json(instanceToPlain(quotes, { groups: [ADMIN] }));
});

/**
 * @openapi
 *  quotes/add:
 *    post:
 *      summary: Add quote
 *      description: Adds a new quote to the system
 *      requestBody:
 *        content:
 *          application/json:
 *            schema:
 *              description: Describes the new quote to add
 *              type: object
 *              required:
 *              - pickUp
 *              - dropOff
 *              properties:
 *                pickUp:
 *                  type: object
 *                  properties:
 *                    lat:
 *                      description: The latitude of the pickup location
 *                      type: number
 *                      minimum: -90
 *                      maximum: 90
 *                      example: 43.6532
 *                    lng:
 *                      description: The longitude of the pickup location
 *                      type: number
 *                      minimum: -90
 *                      maximum: 90
 *                      example: 43.6532
 *                dropOff:
 *                  type: object
 *                  properties:
 *                    lat:
 *                      description: The latitude of the pickup location
 *                      type: number
 *                      minimum: -90
 *                      maximum: 90
 *                      example: 43.6532
 *                    lng:
 *                      description: The longitude of the pickup location
 *                      type: number
 *                      minimum: -90
 *                      maximum: 90
 *                      example: 43.6532
 *            examples:
 *              quote:
 *                summary: A quote to add
 *                value:
 *                  pickUp:
 *                    lat: 43.65062031183803
 *                    lng: -79.39772567670434
 *                  dropOff:
 *                    lat: 43.66062031183803
 *                    lng: -79.40772567670434
 *                  priceInCents: 299
 *      responses:
 *        200:
 *          description: The newly-added quote
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/AddQuoteResponse'
 */
router.post('/add', async function (req, res, next) {
    const quoteToAdd = plainToInstance(Quote, req.body, { groups: [CREATE] });

    const validationErrors = await validate(quoteToAdd);
    if(validationErrors.length > 0) {
      const httpError = new BadRequest();
      httpError.properties = validationErrors;
      return next(httpError);
    }

    const service:QuoteService = req.app.get('getService')(QuoteService);

    let quote = null;

    try {
      quote = await service.add(quoteToAdd);
    } catch(err: unknown) {
      if(err instanceof PickUpOutOfDeliveryZoneException) {
        return next(new BadRequest(err.message));
      } else if(err instanceof DropOffOutOfDeliveryZoneException) {
        return next(new BadRequest(err.message));
      } else {
        return next(err);
      }
    }

    res.json(instanceToPlain(quote, { groups: [USER] }));
  }
);

export default router;
