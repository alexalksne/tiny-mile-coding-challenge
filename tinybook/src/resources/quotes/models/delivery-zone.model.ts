import { Point } from "./common";
import { getDistance } from 'geolib';

export type Circle = {
  origin: Point;
  radiusInMeters: number;
  contains(point: Point): boolean;
}

export class DeliveryZoneOrigin implements Point {
  constructor(public lat: number, public lng: number) {};
}

export class DeliveryZone implements Circle {
  constructor(public origin: DeliveryZoneOrigin, public radiusInMeters: number) {};

  contains(point: Point) {
    return getDistance(this.origin, point) <= this.radiusInMeters;
  }
}
