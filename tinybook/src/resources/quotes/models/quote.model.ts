import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn } from "typeorm";
import { Exclude, Expose, Type } from "class-transformer";
import { IsNumber, IsOptional } from 'class-validator';
import { Point } from "./common";
import { ADMIN, CREATED_AT, UPDATED_AT, USER, CREATE } from "../../../constants";

export enum RobotStatus {
  OUT_OF_SERVICE = "out-of-service",
  DELIVERING = "delivering",
  READY_FOR_DELIVERY = "ready-for-delivery"
};

@Entity({ schema: 'tinybook' })
@Exclude()
export class Destination implements Point {
  @Expose({ groups: [ADMIN] })
  @PrimaryGeneratedColumn()
  id!: number;

  @IsNumber()
  @Expose({ groups: [ADMIN, USER, CREATE] })
  @Column({ type: 'float' })
  lat!: number;

  @IsNumber()
  @Expose({ groups: [ADMIN, USER, CREATE] })
  @Column({ type: 'float' })
  lng!: number;

  @CreateDateColumn({ name: CREATED_AT })
  createdAt!: Date;

  @UpdateDateColumn({ name: UPDATED_AT })
  updatedAt!: Date;
}

/**
 * @openapi
 *  components:
 *    schemas:
 *      Quote-AdminView:
 *        type: object
 *        properties:
 *          id:
 *            description: The ID for this quote
 *            type: string
 *            example: 3
 *          pickUp:
 *            type: object
 *            properties:
 *              id:
 *                description: The ID for this pick up
 *                type: string
 *                example: 3
 *              lat:
 *                description: The latitude of the pickup location
 *                type: number
 *                minimum: -90
 *                maximum: 90
 *                example: 43.6532
 *              lng:
 *                description: The longitude of the pickup location
 *                type: number
 *                minimum: -90
 *                maximum: 90
 *                example: 43.6532
 *          dropOff:
 *            type: object
 *            properties:
 *              id:
 *                description: The ID for this drop off
 *                type: string
 *                example: 3
 *              lat:
 *                description: The latitude of the pickup location
 *                type: number
 *                minimum: -90
 *                maximum: 90
 *                example: 43.6532
 *              lng:
 *                description: The longitude of the pickup location
 *                type: number
 *                minimum: -90
 *                maximum: 90
 *                example: 43.6532
 *          priceInCents:
 *            description: The price for the delivery in cents
 *            type: number
 *            minimum: 0
 *            example: 299
 *      Quote-UserView:
 *        type: object
 *        properties:
 *          id:
 *            description: The ID for this quote
 *            type: string
 *            example: 3
 *          pickUp:
 *            type: object
 *            properties:
 *              lat:
 *                $ref: '#/components/schemas/Quote-AdminView/properties/pickUp/properties/lat'
 *              lng:
 *                $ref: '#/components/schemas/Quote-AdminView/properties/pickUp/properties/lat'
 *          dropOff:
 *            type: object
 *            properties:
 *              lat:
 *                $ref: '#/components/schemas/Quote-AdminView/properties/dropOff/properties/lat'
 *              lng:
 *                $ref: '#/components/schemas/Quote-AdminView/properties/dropOff/properties/lat'
 *          priceInCents:
 *            $ref: '#/components/schemas/Quote-AdminView/properties/priceInCents'
 *      GetQuotesResponse:
 *        description: A list of quotes for potential Tiny Mile orders
 *        type: array
 *        items:
 *          $ref: '#/components/schemas/Quote-AdminView'
 *      AddQuoteResponse:
 *        $ref: '#/components/schemas/Quote-UserView'
 */
@Entity({ schema: 'tinybook' })
@Exclude()
export class Quote {
  @Expose({ groups: [ADMIN, USER] })
  @PrimaryGeneratedColumn()
  id!: number;

  @Expose({ groups: [ADMIN, USER, CREATE] })
  @Type(() => Destination)
  @OneToOne(() => Destination)
  @JoinColumn({ name: 'pick_up_id' })
  pickUp!: Destination;
  
  @Expose({ groups: [ADMIN, USER, CREATE] })
  @Type(() => Destination)
  @OneToOne(() => Destination)
  @JoinColumn({ name: 'drop_off_id' })
  dropOff!: Destination;

  @IsNumber()
  @IsOptional()
  @Expose({ groups: [ADMIN, USER] })
  @Column({  name: 'price_in_cents', type: 'float' })
  priceInCents!: number;

  @CreateDateColumn({ name: CREATED_AT })
  createdAt!: Date;

  @UpdateDateColumn({ name: UPDATED_AT })
  updatedAt!: Date;
}
