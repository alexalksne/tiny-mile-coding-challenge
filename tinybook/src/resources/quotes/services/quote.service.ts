import { Debugger } from 'debug';
import { Connection, Repository } from 'typeorm';
import { Point } from '../models/common';
import { DeliveryZone } from '../models/delivery-zone.model';
import { Destination, Quote } from '../models/quote.model';

import { getDistance } from 'geolib';

abstract class OutOfDeliveryZoneException extends Error { }

export class PickUpOutOfDeliveryZoneException extends OutOfDeliveryZoneException {
  constructor(pickUp: Point) {
    super(`Tried to set a pick up point to (${pickUp.lat}, ${pickUp.lng}) which is outside the delivery zone.`);
    this.name = 'PickUpOutOfDeliveryZoneException';
  }
}

export class DropOffOutOfDeliveryZoneException extends OutOfDeliveryZoneException {
  constructor(dropOff: Point) {
    super(`Tried to set a drop off point to (${dropOff.lat}, ${dropOff.lng}) which is outside the delivery zone.`);
    this.name = 'DropOffOutOfDeliveryZoneException';
  }
}

class QuoteService {
  constructor(
    private debug: Debugger,
    private connection: Connection,
    private quoteRepository: Repository<Quote>,
    private deliveryZone: DeliveryZone,
    private rateCentsPerKm: number
  ) { };

  async getAll(): Promise<Quote[]> {
    return this.quoteRepository.find({ relations: ['pickUp', 'dropOff'] })
  }

  async add(params: Record<string, any>): Promise<Quote> {
    const quote = await this.connection.transaction(transactionalEntityManager => {
      const quote = new Quote();

      const pickUp = new Destination();
      pickUp.lat = params.pickUp.lat;
      pickUp.lng = params.pickUp.lng;

      if (!this.withinDeliveryZone(pickUp)) {
        throw new PickUpOutOfDeliveryZoneException(pickUp);
      }

      transactionalEntityManager.save(pickUp);
      quote.pickUp = pickUp;

      const dropOff = new Destination();
      dropOff.lat = params.dropOff.lat;
      dropOff.lng = params.dropOff.lng;

      if (!this.withinDeliveryZone(dropOff)) {
        throw new DropOffOutOfDeliveryZoneException(dropOff);
      }

      transactionalEntityManager.save(dropOff);
      quote.dropOff = dropOff;

      quote.priceInCents = Math.round(this.getPriceEstimateInCents(pickUp, dropOff));

      return transactionalEntityManager.save(quote);
    });

    return quote;
  }

  private withinDeliveryZone(pickUp: Destination) {
    return this.deliveryZone.contains(pickUp);
  }

  private getPriceEstimateInCents(pickUp: Destination, dropOff: Destination): number {
    const travelDistInMeters = getDistance(pickUp, dropOff);
    return (travelDistInMeters / 1000) * this.rateCentsPerKm;
  }
}

export default QuoteService;
