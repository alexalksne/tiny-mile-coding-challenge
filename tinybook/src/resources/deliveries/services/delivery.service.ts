import { Debugger } from 'debug';
import { Connection, Repository } from 'typeorm';
import { Quote } from '../../quotes/models/quote.model';
import { Robot, RobotStatus } from '../../robots/models/robot.model';
import { Delivery } from '../models/delivery.model';

export class QuoteNotFoundExeption extends Error {
  constructor(quoteId: number) {
    super(`The quote with id ${quoteId} doesn't exist.`);
    this.name = 'QuoteNotFoundExeption';
  }
}

export class NoFreeRobotsAvailableException extends Error {
  constructor() {
    super(`No free robot found to accept delivery.`);
    this.name = 'NoFreeRobotsAvailableException';
  }
}

export class DeliveryAlreadyCreatedException extends Error {
  constructor(quoteId: number) {
    super(`A delivery for quote ${quoteId} has already been created.`);
    this.name = 'DeliveryAlreadyCreatedException';
  }
}

class DeliveryService {
  constructor(
    private debug: Debugger,
    private connection: Connection,
    private quoteRepository: Repository<Quote>,
    private robotRepository: Repository<Robot>,
    private deliveryRepository: Repository<Delivery>,
  ) { };

  async getAll(): Promise<Delivery[]> {
    return this.deliveryRepository.find({ relations: ['robot', 'quote', 'quote.pickUp', 'quote.dropOff'] });
  }

  async add(params: Record<string, any>): Promise<Delivery> {
    const delivery = await this.connection.transaction(async transactionalEntityManager => {
      const quote = await this.quoteRepository.findOne({ id: params.quoteId }, {relations: ['pickUp', 'dropOff']});
      if(!quote) {
        throw new QuoteNotFoundExeption(params.quoteId);
      }

      let delivery = await this.deliveryRepository.findOne({ quote });
      if(delivery) {
        throw new DeliveryAlreadyCreatedException(params.quoteId);
      }

      const robot = await this.robotRepository.findOne({ status: RobotStatus.READY_FOR_DELIVERY });
      if(!robot) {
        throw new NoFreeRobotsAvailableException();
      }

      robot.status = RobotStatus.DELIVERING;
      await transactionalEntityManager.save(robot);
  
      delivery = new Delivery();
      delivery.quote = quote;
      delivery.robot = robot;
  
      return transactionalEntityManager.save(delivery);
    });

    return delivery;
  }
}

export default DeliveryService;
