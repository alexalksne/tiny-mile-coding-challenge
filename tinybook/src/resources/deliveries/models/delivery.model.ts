import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn } from "typeorm";
import { Exclude, Expose, Type } from "class-transformer";
import { ADMIN, CREATED_AT, UPDATED_AT, USER, CREATE } from "../../../constants";
import { Robot } from "../../robots/models/robot.model";
import { Quote } from "../../quotes/models/quote.model";

/**
 * @openapi
 *  components:
 *    schemas:
 *      Delivery-AdminView:
 *        type: object
 *        properties:
 *          id:
 *            description: The ID for this delivery
 *            type: string
 *            example: 3
 *          robot:
 *            $ref: '#/components/schemas/Robot'
 *          quote:
 *            $ref: '#/components/schemas/Quote-AdminView'
 *      Delivery-UserView:
 *        type: object
 *        properties:
 *          id:
 *            description: The ID for this delivery
 *            type: string
 *            example: 3
 *          robot:
 *            $ref: '#/components/schemas/Robot'
 *          quote:
 *            $ref: '#/components/schemas/Quote-UserView'
 *      GetDeliveriesResponse:
 *        description: A list of deliveries in progress
 *        type: array
 *        items:
 *          $ref: '#/components/schemas/Delivery-AdminView'
 *      AddDeliveryResponse:
 *        $ref: '#/components/schemas/Delivery-UserView'
 */
@Entity({ schema: 'tinybook' })
@Exclude()
export class Delivery {
  @Expose({ groups: [ADMIN, USER] })
  @PrimaryGeneratedColumn()
  id!: number;

  @Expose({ groups: [ADMIN, USER] })
  @Type(() => Robot)
  @OneToOne(() => Robot)
  @JoinColumn({ name: 'robot_id' })
  robot!: Robot;

  @Expose({ groups: [ADMIN, USER] })
  @Type(() => Quote)
  @OneToOne(() => Quote)
  @JoinColumn({ name: 'quote_id' })
  quote!: Quote;

  @CreateDateColumn({ name: CREATED_AT })
  createdAt!: Date;

  @UpdateDateColumn({ name: UPDATED_AT })
  updatedAt!: Date;
}
