import { Router } from 'express';
import { instanceToPlain } from 'class-transformer';
import { ADMIN, USER } from '../../../constants';
import DeliveryService, { DeliveryAlreadyCreatedException, NoFreeRobotsAvailableException, QuoteNotFoundExeption } from '../services/delivery.service';
import { BadRequest } from 'http-errors'

const router = Router();

/**
 * @openapi
 *  deliveries:
 *    get:
 *      summary: Get deliveries
 *      description: Gets all deliveries in the system
 *      responses:
 *        200:
 *          description: An array of all the deliveries
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/GetDeliveriesResponse'
 */
router.get('/', async function (req, res) {
  const service:DeliveryService = req.app.get('getService')(DeliveryService);

  const deliveries = await service.getAll();

  res.json(instanceToPlain(deliveries, { groups: [ADMIN] }));
});

/**
 * @openapi
 *  deliveries/add:
 *    post:
 *      summary: Create new delivery
 *      description: Adds a new delivery to the system based on a previously-given quote
 *      parameters:
 *      - description: The ID of the quote
 *        name: quoteId
 *        in: query
 *        required: true
 *        schema:
 *          type: integer
 *          minimum: 0
 *          example: 3
 *      responses:
 *        200:
 *          description: The newly-added delivery
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/AddDeliveryResponse'
 */
router.post('/add', async function (req, res, next) {
    const quoteId = req.query['quoteId'];

    const service:DeliveryService = req.app.get('getService')(DeliveryService);

    let delivery = null;

    try {
      delivery = await service.add({ quoteId });
    } catch(err: unknown) {
      if(err instanceof QuoteNotFoundExeption) {
        return next(new BadRequest(err.message));
      } else if(err instanceof NoFreeRobotsAvailableException) {
        return next(new BadRequest(err.message));
      } else if(err instanceof DeliveryAlreadyCreatedException) {
        return next(new BadRequest(err.message));
      } else {
        return next(err);
      }
    }

    res.json(instanceToPlain(delivery, { groups: [USER] }));
  }
);

export default router;
