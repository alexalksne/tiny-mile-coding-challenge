import { Router } from 'express';
import { Env } from '../../..';

const router = Router();

/**
 * @openapi
 *  :
 *    get:
 *      summary: Identify service
 *      description: Returns a message explaining what the service is
 *      responses:
 *        200:
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  message:
 *                    description: A message from the service explaining its function
 *                    type: string
 *                    example: Hi! I'm X-Service and I do Y!
 */
router.get('/', async function (req, res) {
  const env:Env = req.app.get('env');
  const serviceName = env.SERVICE_NAME;
  res.json({ message: `Welcome to the ${serviceName} service.` });
});

export default router;
