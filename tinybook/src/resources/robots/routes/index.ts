import { Router } from 'express';
import RobotService from '../services/robot.service';
import { instanceToPlain, plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { Robot } from '../models/robot.model';
import { BadRequest } from 'http-errors';
import { CREATE, USER } from '../../../constants';

const router = Router();

/**
 * @openapi
 *  robots:
 *    get:
 *      summary: Get robots
 *      description: Gets all robots in the system
 *      responses:
 *        200:
 *          description: An array of all the robots
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/GetRobotsResponse'
 */
router.get('/', async function (req, res) {
  const service:RobotService = req.app.get('getService')(RobotService);

  const robots = await service.getAll();

  res.json(instanceToPlain(robots, { groups: [USER] }));
});

/**
 * @openapi
 *  robots/add:
 *    post:
 *      summary: Add robot
 *      description: Adds a new robot to the system
 *      requestBody:
 *        content:
 *          application/json:
 *            schema:
 *              description: Describes the new robot to add
 *              type: object
 *              required:
 *              - lat
 *              - lng
 *              properties:
 *                lat:
 *                  description: The latitude of this robot
 *                  type: number
 *                  minimum: -90
 *                  maximum: 90
 *                  example: 43.6532
 *                lng:
 *                  description: The longitude of this robot
 *                  type: number
 *                  minimum: -180
 *                  maximum: 180
 *                  example: 79.3832
 *            examples:
 *              robot:
 *                summary: A robot to add
 *                value:
 *                  lat: 43.6532
 *                  lng: 79.3832
 *      responses:
 *        200:
 *          description: The newly-added robot
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/AddRobotResponse'
 */
router.post('/add', async function (req, res, next) {
    const robotToAdd = plainToInstance(Robot, req.body, { groups: [CREATE] });

    const validationErrors = await validate(robotToAdd);
    if(validationErrors.length > 0) {
      const httpError = new BadRequest();
      httpError.properties = validationErrors;
      return next(httpError);
    }

    const service:RobotService = req.app.get('getService')(RobotService);

    const robot = await service.add(robotToAdd);

    res.json(instanceToPlain(robot, { groups: [USER] }));
  }
);

export default router;
