import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Exclude, Expose } from "class-transformer";
import { IsNumber } from 'class-validator';
import { ADMIN, CREATE, CREATED_AT, UPDATED_AT, USER } from "../../../constants";

export enum RobotStatus {
  OUT_OF_SERVICE = "out-of-service",
  DELIVERING = "delivering",
  READY_FOR_DELIVERY = "ready-for-delivery"
};

/**
 * @openapi
 *  components:
 *    schemas:
 *      Robot:
 *        type: object
 *        properties:
 *          id:
 *            description: The unique ID for this robot
 *            type: integer
 *            minimum: 0
 *            example: 3
 *          lat:
 *            description: The latitude of this robot
 *            type: number
 *            minimum: -90
 *            maximum: 90
 *            example: 43.6532
 *          lng:
 *            description: The longitude of this robot
 *            type: number
 *            minimum: -180
 *            maximum: 180
 *            example: 79.3832
 *          status:
 *            description: The status of this robot
 *            type: string
 *            enum:
 *            - out-of-service
 *            - delivering
 *            - ready-for-delivery
 *            example: ready-for-delivery
 *      GetRobotsResponse:
 *        description: A list of delivery robots operated by TinyMile
 *        type: array
 *        items:
 *          $ref: '#/components/schemas/Robot'
 *      AddRobotResponse:
 *        $ref: '#/components/schemas/Robot'
 */
@Entity({ schema: 'tinybook' })
@Exclude()
export class Robot {
  @Expose({ groups: [ADMIN, USER] })
  @PrimaryGeneratedColumn()
  id!: number;

  @IsNumber()
  @Expose({ groups: [ADMIN, USER, CREATE] })
  @Column({ type: 'float' })
  lat!: number;

  @IsNumber()
  @Expose({ groups: [ADMIN, USER, CREATE] })
  @Column({ type: 'float' })
  lng!: number;

  @Expose({ groups: [ADMIN, USER] })
  @Column({
    type: "enum",
    enum: RobotStatus,
    default: RobotStatus.READY_FOR_DELIVERY
  })
  status!: RobotStatus;

  @CreateDateColumn({ name: CREATED_AT })
  createdAt!: Date;

  @UpdateDateColumn({ name: UPDATED_AT })
  updatedAt!: Date;
}
