import { Debugger } from 'debug';
import { Repository } from 'typeorm';
import { Robot } from '../models/robot.model';

class RobotService {
    constructor(private debug: Debugger, private robotRepository: Repository<Robot>){};

    async getAll(): Promise<Robot[]> {
      return this.robotRepository.find();
    }

    async add(params: Record<string, any>): Promise<Robot> {
      const robot = new Robot();
      robot.lat = params.lat;
      robot.lng = params.lng;

      return this.robotRepository.save(robot);
    }
}

export default RobotService;
