import { appFactory } from './app';
import Debug from 'debug';
import http from 'http';
import gracefulShutdown from 'http-graceful-shutdown';
import { AddressInfo } from 'net'
import { cleanEnv, str, port } from 'envalid';
import { createConnection } from 'typeorm';
import { DEVELOPMENT, PRODUCTION } from './constants';

type Override<T1, T2> = Omit<T1, keyof T2> & T2;
type RuntimeEnvironments = typeof DEVELOPMENT | typeof PRODUCTION;

type EnvRaw = {
  NODE_ENV: RuntimeEnvironments,
  PORT: number,
  SERVICE_NAME: string,
  DELIVERY_ZONE_ORIGIN_LAT: string,
  DELIVERY_ZONE_ORIGIN_LNG: string,
  DELIVERY_ZONE_RADIUS_IN_METERS: string,
  DELIVERY_RATE_CENTS_PER_KM: string,
  TYPEORM_LOGGING: string,
  TYPEORM_CONNECTION: string,
  TYPEORM_HOST: string,
  TYPEORM_PORT: string,
  TYPEORM_USERNAME: string,
  TYPEORM_PASSWORD: string,
  TYPEORM_DATABASE: string,
  TYPEORM_SYNCHRONIZE: string,
  TYPEORM_ENTITIES?: string
}

type DbConnectionType = 'postgres';
export type Env = Override<EnvRaw, { 
  NODE_ENV: RuntimeEnvironments,
  DELIVERY_ZONE_ORIGIN_LAT: number,
  DELIVERY_ZONE_ORIGIN_LNG: number,
  DELIVERY_ZONE_RADIUS_IN_METERS: number,
  DELIVERY_RATE_CENTS_PER_KM: number,
  TYPEORM_LOGGING: boolean,
  TYPEORM_CONNECTION: DbConnectionType,
  TYPEORM_PORT: number,
  TYPEORM_SYNCHRONIZE: boolean
}>

async function main() {
  const envRaw = cleanEnv(process.env, {
    NODE_ENV: str({ choices: [DEVELOPMENT, PRODUCTION] }),
    PORT: port(),
    SERVICE_NAME: str(),
    DELIVERY_ZONE_ORIGIN_LAT: str(),
    DELIVERY_ZONE_ORIGIN_LNG: str(),
    DELIVERY_ZONE_RADIUS_IN_METERS: str(),
    DELIVERY_RATE_CENTS_PER_KM: str(),
    TYPEORM_LOGGING: str(),
    TYPEORM_CONNECTION: str({ choices: ['postgres'] }),
    TYPEORM_HOST: str(),
    TYPEORM_PORT: str(),
    TYPEORM_USERNAME: str(),
    TYPEORM_PASSWORD: str(),
    TYPEORM_DATABASE: str(),
    TYPEORM_SYNCHRONIZE: str(),
    TYPEORM_ENTITIES: str({ default: undefined })
  });

  const env:Env = {
    NODE_ENV: envRaw.NODE_ENV as RuntimeEnvironments,
    PORT: envRaw.PORT,
    SERVICE_NAME: envRaw.SERVICE_NAME,
    DELIVERY_ZONE_ORIGIN_LAT: Number(envRaw.DELIVERY_ZONE_ORIGIN_LAT),
    DELIVERY_ZONE_ORIGIN_LNG: Number(envRaw.DELIVERY_ZONE_ORIGIN_LNG),
    DELIVERY_ZONE_RADIUS_IN_METERS: Number(envRaw.DELIVERY_ZONE_RADIUS_IN_METERS),
    DELIVERY_RATE_CENTS_PER_KM: Number(envRaw.DELIVERY_RATE_CENTS_PER_KM),
    TYPEORM_LOGGING: envRaw.TYPEORM_LOGGING.toLowerCase() === 'true',
    TYPEORM_CONNECTION: envRaw.TYPEORM_CONNECTION as DbConnectionType,
    TYPEORM_HOST: envRaw.TYPEORM_HOST,
    TYPEORM_PORT: Number(envRaw.TYPEORM_PORT),
    TYPEORM_USERNAME: envRaw.TYPEORM_USERNAME,
    TYPEORM_PASSWORD: envRaw.TYPEORM_PASSWORD,
    TYPEORM_DATABASE: envRaw.TYPEORM_DATABASE,
    TYPEORM_SYNCHRONIZE: envRaw.TYPEORM_SYNCHRONIZE.toLowerCase() === 'true',
    TYPEORM_ENTITIES: envRaw.TYPEORM_ENTITIES
  }

  const debug = Debug(`${env.SERVICE_NAME}:server`);

  const connection = await createConnection({
    logging: env.TYPEORM_LOGGING,
    type: env.TYPEORM_CONNECTION,
    host: env.TYPEORM_HOST,
    port: env.TYPEORM_PORT,
    username: env.TYPEORM_USERNAME,
    password: env.TYPEORM_PASSWORD,
    database: env.TYPEORM_DATABASE,
    synchronize: env.TYPEORM_SYNCHRONIZE,
    entities: env.TYPEORM_ENTITIES ? [ env.TYPEORM_ENTITIES ] : []
  });
  
  function onError(error: NodeJS.ErrnoException) {
    if (error.syscall !== 'listen') throw error;
  
    const port = (server.address() as AddressInfo).port;
    if(error.code === 'EACCES') {
      console.error(`Port ${port} requires elevated privileges.`);
      process.exit(1);
    } else if(error.code === 'EADDRINUSE') {
      console.error(`Port ${port} is already in use.`)
      process.exit(1);
    } else {
      throw error;
    }
  }
  
  function onListening() {
    const port = (server.address() as AddressInfo).port;
    debug(`Listening on port ${port}.`);
  }
  
  function onShutdown() {
    return new Promise<void>(async (resolve, reject) => {
      await connection.close();
      debug('Closed db connection.');
      resolve();
    });
  }

  const app = appFactory(env, connection);
  const server = http.createServer(app)
    .listen(env.PORT)
    .on('error', onError)
    .on('listening', onListening);
  
  gracefulShutdown(server, { 
    timeout: 10000,
    onShutdown: onShutdown,
    development: env.NODE_ENV === DEVELOPMENT 
  });  
}

main();
