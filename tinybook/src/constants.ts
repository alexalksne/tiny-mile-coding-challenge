export const DEVELOPMENT = 'development';
export const PRODUCTION = 'production';
export const USER = 'user';
export const ADMIN = 'admin';
export const CREATE = 'create';
export const CREATED_AT = 'created_at';
export const UPDATED_AT = 'updated_at';
