import { NextFunction, Request, Response } from 'express';
import { NotFound } from 'http-errors';

export default function(req: Request, res: Response, next: NextFunction) {
  next(new NotFound());
};
