import { NextFunction, Request, Response } from 'express';
import createError from 'http-errors';
import { PRODUCTION } from '../constants';
import { Env } from '..';

export default function(err: unknown, req: Request, res: Response, next: NextFunction) {
  const env = req.app.get('env') as Env;

  if(createError.isHttpError(err)) {
    res.status(err.status);
    res.json(env.NODE_ENV === PRODUCTION ? { message: err.message } : err);
  } else {
    res.status(500);
    res.json(env.NODE_ENV === PRODUCTION ? { message: 'Internal Server Error' } : { message: err });
  }
};
