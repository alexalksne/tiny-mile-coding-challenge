import swaggerJsDoc, { OAS3Options } from 'swagger-jsdoc';
import { writeFile } from 'fs/promises';

const options: OAS3Options = {
  apis: ['./src/**/*.ts'],
  definition: {
    openapi: '3.1.0',
    servers: [{
      url: 'http://{host}:{port}',
      variables: {
        host: {
          enum: ['localhost'],
          default: 'localhost'
        },
        port: {
          enum: ['3000'],
          default: '3000'
        }
      }
    }],
    info: {
      title: 'tinybook',
      version: '0.1.0',
      description: 'Allows you to book deliveries using the TinyMile fleet',
      contact: {
        name: 'Alex Alksne',
        email: 'alex.alksne@gmail.com'
      },
      license: {
        name: 'UNLICENSED'
      },
    },
  },
};

const main = async () => {
  const openapiSpecification = swaggerJsDoc(options);
  await writeFile('./openapi/openapi-spec.json', JSON.stringify(openapiSpecification, null, 2));
}

main();
