# Tiny Mile System
Encapsulates the Tiny Mile system composed of a booking system as well as a schema migrator service.

## Development
All development is done against a locally-running docker container. The tinybook service is mapped into the container using a volume; coupled with nodemon this allows for rapid development in a standardized environment.

### Getting started
This runs the tinybook service in development mode; you'll need Docker installed:
```bash
cp .env.example .env
npm run start:dev
```

To run the container in production mode, simply run `npm start`.

## Migrations
In development mode (`npm run start:dev`), typeorm is set to automatically synchronize entities with the database. To capture the changes made to your models and have them reflected in production, run `npm run migration:generate FILENAME_FOR_MIGRATION`. This will create a migration file in the migrator service (`../migrator/migrations`). Then run `npm run typeorm:migration:run` to update the db to the latest schema.

## OpenApi
To inspect the API run `npm run openapi:view` and visit `http://localhost:5000`. You can also load `./openapi/openapi-spec.json` into a program like Postman to run some example calls.

## Features
- **docker and docker-compose support**: Each service has their own Dockerfile that is used in production. All services are "orchestrated" (weakly) via docker-compose. The LTS version of node is chosen for the Dockerfile as a best practice.
- **migration server**: when the system is started via `docker-compose` the migration service automatically runs before `tinybook` and implements any outstanding migrations. Race conditions between services starting up are avoided by a combination of `depends_on` docker-compose keywords and a custom `wait-till-server-down` script that waits for the migration server to finish running before starting `tinybook`. `wait-till-server-down` is like `wait-for-it` (https://github.com/vishnubob/wait-for-it) but the opposite.
- **detailed healthcheck**: All services (tinybook, migrator, postgres, and pgadmin) expose a healthcheck. In the case of `tinybook` the healthcheck gives a detailed breakdown of the health of `tinybook` as well as all services it depends on. If postgres is down, the `tinybook` healthcheck will report this and not simply state that it's not healthy.
- **graceful shutdown**: prevents new connections being made and gives existing connections a grace period to close
- **transaction support**: complicated SQL commands are run as transactions so you're never left in an invalid state if something goes wrong midway through the API call
- **env var validation**: the server won't start unless all required env vars are set and adhere to the expected schema
- **environment aware runtime**: the server runs differently depending on whether it's in `development` or `production` mode
- **helmet npm module**: "Helmet helps you secure your Express apps by setting various HTTP headers. It's not a silver bullet, but it can help!"
- **openapi support**: annotate your code using jsdoc-like comments using `@openapi` which then get picked up by a script that aggregates and reconciles them into a single openapi (v3) file for consumption in tools like ReDoc or Postman.
- **dependency injection**: very simple, and easy to understand, implementation that doesn't depend on libraries (like inversify).
- **typeorm support**: automatically synchronizes models in development and makes use of typical typeorm features to speed up development and improve readability
- **class-validator and class-transformer support**: annotate your models with decorators that describe invariants (@IsNumber, etc.) and give serialization/deserialization instructions.
- **sensible directory structure**: each resource has its own `models`, `routes`, and `services` folder for easy navigation.
