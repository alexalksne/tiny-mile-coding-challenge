const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class addRobot1641844367441 {
    name = 'addRobot1641844367441'

    async up(queryRunner) {
        await queryRunner.query(`
            CREATE TABLE "tinybook"."robot" (
                "id" SERIAL NOT NULL,
                "lat" integer NOT NULL,
                "lng" integer NOT NULL,
                "created_at" TIMESTAMP NOT NULL DEFAULT now(),
                "updated_at" TIMESTAMP NOT NULL DEFAULT now(),
                CONSTRAINT "PK_b4fbeccee808e9f8ffe2540b0c2" PRIMARY KEY ("id")
            )
        `);
    }

    async down(queryRunner) {
        await queryRunner.query(`
            DROP TABLE "tinybook"."robot"
        `);
    }
}
