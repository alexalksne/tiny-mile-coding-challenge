import express from 'express';

const router = express.Router();

router.get('/healthcheck', function(req, res, next) {
  res.status(200).json({ ok: true });
});

export default router;
