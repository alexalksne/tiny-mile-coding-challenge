import createError from 'http-errors';
import express, { Request, Response, NextFunction } from 'express';
import logger from 'morgan';
import helmet from 'helmet';
import healthcheck from './healthcheck';

type Env = Readonly<{
  NODE_ENV: 'development' | 'production',
  PORT: number
}>

const appFactory = (env: Env) => { 
  const app = express()
  .set('env', env)
  .set('json spaces', 2)
  .set('views', null)
  .set('view cache', false)
  .use(helmet())
  .use(logger('short'))
  .use('/', healthcheck)
  .use(function(req, res, next) {
    next(createError(404));
  })
  .use(function(err: unknown, req: Request, res: Response, next: NextFunction) {
    res.json({error: 500});
  });

  return app;
}

export { appFactory };
