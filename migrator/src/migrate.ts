import { createConnection } from 'typeorm'

async function migrate() {
  const connection = await createConnection({
    type: 'postgres',
    host: process.env.TYPEORM_HOST,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    port: Number(process.env.TYPEORM_PORT),
    logging: (process.env.TYPEORM_LOGGING || '') === 'true',
    migrations: [process.env.TYPEORM_MIGRATIONS || ''],
    schema: process.env.TYPEORM_SCHEMA
  });
  return connection.runMigrations();
}

export default migrate;
