import { appFactory } from './app';
import Debug from 'debug';
import http from 'http';
import { AddressInfo } from 'net'
import { cleanEnv, str, port, bool } from 'envalid';
import migrate from './migrate';

const debug = Debug('challenge:server');

type Env = Readonly<{
  NODE_ENV: 'development' | 'production',
  PORT: number,
  SKIP_MIGRATIONS: boolean
}>

function main() {
  const env = cleanEnv(process.env, {
    NODE_ENV: str({ choices: ['development', 'production'] }),
    PORT: port(),
    SKIP_MIGRATIONS: bool()
  }) as Env;

  if(env.SKIP_MIGRATIONS) {
    debug(`Skipping migrations.`);
    process.exit(0);
  }
  
  function onError(error: NodeJS.ErrnoException) {
    if (error.syscall !== 'listen') throw error;
  
    const port = (server.address() as AddressInfo).port;
    if(error.code === 'EACCES') {
      console.error(`Port ${port} requires elevated privileges.`);
      process.exit(1);
    } else if(error.code === 'EADDRINUSE') {
      console.error(`Port ${port} is already in use.`)
      process.exit(1);
    } else {
      throw error;
    }
  }
  
  async function onListening() {
    const port = (server.address() as AddressInfo).port;
    debug(`Listening on port ${port}.`);

    debug('Beginning migration');

    await migrate();
    debug('Done running migration job.');

    process.exit(0);
  }

  const app = appFactory(env);
  const server = http.createServer(app)
    .listen(env.PORT)
    .on('error', onError)
    .on('listening', onListening);
}

main();
